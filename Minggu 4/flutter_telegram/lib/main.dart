import 'package:flutter/material.dart';
import 'package:flutter_telegram/routes/route_names.dart';
import 'package:flutter_telegram/routes/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      routes: routes,
      debugShowCheckedModeBanner: false,
      initialRoute: RouteNames.login,
      theme: ThemeData(primarySwatch: Colors.blue),
    );
  }
}
