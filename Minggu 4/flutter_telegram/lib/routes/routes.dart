import 'package:flutter/material.dart';
import 'package:flutter_telegram/routes/route_names.dart';
import 'package:flutter_telegram/screens/login_screen.dart';
import 'package:flutter_telegram/screens/pengaturan_screen.dart';
import 'package:flutter_telegram/screens/telegram_screen.dart';

Map<String, WidgetBuilder> routes = {
  RouteNames.home: (context) => const TelegramScreen(),
  RouteNames.login: (context) => const LoginScreen(),
  RouteNames.pengaturan: (context) => const PengaturanScreen(),
};
