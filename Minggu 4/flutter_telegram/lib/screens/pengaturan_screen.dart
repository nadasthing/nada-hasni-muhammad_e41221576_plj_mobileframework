import 'package:flutter/material.dart';
import 'package:flutter_telegram/routes/route_names.dart';

class PengaturanScreen extends StatelessWidget {
  const PengaturanScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Pengaturan'),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
          ),
        ],
        // leading: Icon(Icons.dashboard),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 100,
          ),
          Row(
            children: [
              const SizedBox(width: 20),
              Text(
                'Hello World',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(width: 20),
              Image.asset('assets/images/log-in.png'),
              const SizedBox(width: 20),
            ],
          ),
          SizedBox(height: 100),
          Row(
            children: [
              const SizedBox(width: 20),
              Text(
                'Hello World',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(width: 20),
              Image.asset('assets/images/log-in.png'),
              const SizedBox(width: 20),
            ],
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Colors.white,
        child: Image.asset(
          'assets/images/google.png',
          width: 24,
        ),
      ),
      bottomNavigationBar: Container(
        height: 70,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Icon(Icons.dashboard),
            Icon(Icons.report),
            Icon(Icons.settings),
            Icon(Icons.person),
          ],
        ),
      ),
      // drawer: Drawer(
      //   child: ListView(
      //     children: [
      //       ListTile(
      //         title: Text('Telegram'),
      //         onTap: () {
      //           Navigator.pushNamed(context, RouteNames.home);
      //         },
      //       ),
      //       ListTile(
      //         title: Text('Login'),
      //         onTap: () {
      //           Navigator.pushNamed(context, RouteNames.login);
      //         },
      //       ),
      //       ListTile(
      //         title: Text('Home'),
      //         onTap: () {},
      //       ),
      //     ],
      //   ),
      // ),
    );
  }
}
