import 'package:flutter/material.dart';

class BelajarImageScreen extends StatelessWidget {
  const BelajarImageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Belajar Image')),
      body: Image.asset(
        'assets/images/mount-fuji.jpeg',
        fit: BoxFit.fitWidth,
        width: double.infinity,
      ),
    );
  }
}
