import 'package:flutter/material.dart';
import 'package:flutter_telegram/routes/route_names.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isObsecure = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const SizedBox(height: 100),
              Center(
                child: Image.asset(
                  'assets/images/log-in.png',
                  width: 107,
                ),
              ),
              const SizedBox(height: 8),
              const Text(
                'Start Your Future With Us',
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 20),
              const Text('Login'),
              const SizedBox(height: 5),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Email',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  prefixIcon: const Icon(Icons.email),
                ),
              ),
              const SizedBox(height: 20),
              const Text('Password'),
              const SizedBox(height: 5),
              TextFormField(
                obscureText: isObsecure,
                decoration: InputDecoration(
                  hintText: 'Password',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  suffixIcon: GestureDetector(
                    onTap: () {
                      setState(() {
                        isObsecure = !isObsecure;
                      });
                    },
                    child: Icon(
                        isObsecure ? Icons.visibility : Icons.visibility_off),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, RouteNames.pengaturan);
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xFF0D253F),
                  minimumSize: const Size(100, 60),
                ),
                child: const Text(
                  'Login',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              const SizedBox(height: 10),
              OutlinedButton(
                onPressed: () {
                  Navigator.pushNamed(context, RouteNames.home);
                },
                style: OutlinedButton.styleFrom(
                  minimumSize: const Size(100, 60),
                ),
                child: Row(
                  children: [
                    Image.asset(
                      'assets/images/google.png',
                      height: 24,
                    ),
                    const Spacer(),
                    const Text(
                      'Login With Google',
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF0D253F),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Spacer(),
                  ],
                ),
              ),
              const Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text("Don't have account? "),
                  Text(
                    "Sign Up",
                    style: TextStyle(color: Colors.green),
                  ),
                ],
              ),
              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
