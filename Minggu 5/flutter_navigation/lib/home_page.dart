import 'package:flutter/material.dart';
import 'package:flutter_navigation/about_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Belajar Routing')),
      body: Center(
        child: ElevatedButton(
          onPressed: () => Navigator.pushNamed(context, '/about'),
          child: const Text('Tap Untuk ke About Page'),
        ),
      ),
    );
  }
}
