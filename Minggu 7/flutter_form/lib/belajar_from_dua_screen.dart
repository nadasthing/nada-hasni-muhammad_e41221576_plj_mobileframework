import 'package:flutter/material.dart';

class BelajarFormDuaScreen extends StatefulWidget {
  const BelajarFormDuaScreen({Key? key}) : super(key: key);

  @override
  State<BelajarFormDuaScreen> createState() => _BelajarFormDuaScreenState();
}

class _BelajarFormDuaScreenState extends State<BelajarFormDuaScreen> {
  List<String> agama = [
    'Islam',
    'Kristen Protestan',
    'Kristen Katolik',
    'Hindu',
    'Budha',
  ];
  String? _agama = 'Islam';
  String? _jk = '';

  final _formKey = GlobalKey<FormState>();

  final controllerNama = TextEditingController();
  final controllerPass = TextEditingController();
  final controllerMoto = TextEditingController();

  void _pilihJk(String? value) {
    setState(() {
      _jk = value;
    });
  }

  void _pilihAgama(String? value) {
    setState(() {
      _agama = value;
    });
  }

  void kirimdata() {
    if (!_formKey.currentState!.validate()) return;
    if (_agama.toString().isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Harap pilih agama')));
      return;
    }

    if (_jk.toString().isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Harap pilih agama')));
      return;
    }

    final alertDialog = AlertDialog(
      content: Container(
        height: 200,
        child: Column(
          children: [
            Text('Nama Lengkap : ${controllerNama.text}'),
            Text('Password : ${controllerPass.text}'),
            Text('Jenis Kelamin : $_jk'),
            Text('Agama : $_agama'),
            ElevatedButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('OK'),
            ),
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Data Diri'),
        leading: const Icon(Icons.list),
        backgroundColor: Colors.teal,
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(10),
          children: [
            TextFormField(
              controller: controllerNama,
              decoration: InputDecoration(
                hintText: 'Nama Lengkap',
                labelText: 'Nama Lengkap',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
              validator: (value) {
                if (value.toString().isEmpty) {
                  return 'Harap masukkan nama';
                }
              },
            ),
            const SizedBox(height: 20),
            TextFormField(
              obscureText: true,
              controller: controllerPass,
              decoration: InputDecoration(
                hintText: 'Password',
                labelText: 'Password',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
              validator: (value) {
                if (value.toString().isEmpty) {
                  return 'Harap masukkan password';
                }
              },
            ),
            const SizedBox(height: 20),
            TextFormField(
              maxLines: 3,
              controller: controllerMoto,
              decoration: InputDecoration(
                hintText: 'Moto Hidup',
                labelText: 'Moto Hidup',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
              validator: (value) {
                if (value.toString().isEmpty) {
                  return 'Harap masukkan moto hidup';
                }
              },
            ),
            const SizedBox(height: 20),
            RadioListTile(
              value: 'laki-laki',
              groupValue: _jk,
              onChanged: _pilihJk,
              activeColor: Colors.blue,
              subtitle: Text('Pilih ini jika anda laki-laki'),
            ),
            RadioListTile(
              value: 'perempuan',
              groupValue: _jk,
              onChanged: _pilihJk,
              activeColor: Colors.blue,
              subtitle: Text('Pilih ini jika anda perempuan'),
            ),
            const SizedBox(height: 20),
            Row(
              children: [
                Text('Agama'),
                const SizedBox(width: 15),
                DropdownButton(
                  value: _agama,
                  items: agama
                      .map((value) => DropdownMenuItem(
                            child: Text(value),
                            value: value,
                          ))
                      .toList(),
                  onChanged: _pilihAgama,
                )
              ],
            ),
            ElevatedButton(
              onPressed: kirimdata,
              child: Text('Ok'),
            )
          ],
        ),
      ),
    );
  }
}
