import 'package:flutter/material.dart';

class BelajarFormScreen extends StatefulWidget {
  const BelajarFormScreen({Key? key}) : super(key: key);

  @override
  State<BelajarFormScreen> createState() => _BelajarFormScreenState();
}

class _BelajarFormScreenState extends State<BelajarFormScreen> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiSwitch = true;
  bool nilaiCheckBox = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Belajar Form Flutter"),
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(20),
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                decoration: InputDecoration(
                  hintText: "contoh: Nada Hasni Muhammad",
                  labelText: "Nama Lengkap",
                  icon: const Icon(Icons.people),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Nama tidak boleh kosong';
                  }

                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                obscureText: true,
                decoration: InputDecoration(
                  labelText: "Password",
                  icon: const Icon(Icons.security),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Password tidak boleh kosong';
                  }

                  return null;
                },
              ),
            ),
            CheckboxListTile(
              value: nilaiCheckBox,
              activeColor: Colors.deepPurpleAccent,
              title: const Text('Belajar dasar flutter'),
              subtitle: const Text('Dart, widget, http'),
              onChanged: (value) {
                setState(() {
                  nilaiCheckBox = value ?? false;
                });
              },
            ),
            SwitchListTile(
              value: nilaiSwitch,
              activeColor: Colors.red,
              activeTrackColor: Colors.pink.shade100,
              title: const Text('Backend Programming'),
              subtitle: const Text('Dart, Node.js, PHP, Java, dll'),
              onChanged: (value) {
                setState(() {
                  nilaiSwitch = value;
                });
              },
            ),
            Slider(
              min: 0,
              max: 100,
              value: nilaiSlider,
              onChanged: (value) {
                setState(() {
                  nilaiSlider = value;
                });
              },
            ),
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {}
              },
              child: const Text('Submit'),
            )
          ],
        ),
      ),
    );
  }
}
