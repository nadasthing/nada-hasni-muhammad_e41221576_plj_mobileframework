// * Synchronous to Asynchronous 
void main(List<String> args) async {
  var h = Human();

  print("Luffy");
  print("zoro");
  print("killer");
  getData(h);
  print(h.name);
}

Future<void> getData(Human h) async {
  await Future.delayed(const Duration(seconds: 3));
  h.getData();
  print(h.name);
}

class Human {
  String name = "nama character one piece";

  void getData() {
    name = "hilmy";
    print("get data [done]");
  }
}
