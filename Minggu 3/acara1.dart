void main(List<String> args) {
  // * 1: Function sederhana tanpa return
  tampilkan();

  // * 2: Function sederhana dengan return
  print(munculkanangka());

  // * 3: Function dengan parameter
  print(kalikanDua(6));

  // * 4: Pengiriman parameter lebih dari satu
  print(kalikan(5, 6));

  // *  5: Inisialisasi parameter dengan nilai default
  tampilkanangka(5);

  // *  6: Anonymous function
  print(functionPerkalian(5, 6));
}

tampilkan() {
  print("Hello Peserta Bootcamp");
}

munculkanangka() {
  return 2;
}

kalikanDua(angka) {
  return angka * 2;
}

kalikan(x, y) {
  return x * y;
}

tampilkanangka(n1, {s1: 45}) {
  print(n1); //hasil akan 5 karena initialisasi 5 didalam value tampilkan
  print(s1); //hasil adalah 45 karena dari parameter diisi 45
}

var functionPerkalian = (angka1, angka2) {
  return angka1 * angka2;
};
