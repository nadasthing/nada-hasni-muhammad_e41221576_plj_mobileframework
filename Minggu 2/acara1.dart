void main(List<String> args) {
  //* TIPE DATA STRING ===================
  var sentences = "dart";
  print(sentences[0]); // "d"
  print(sentences[2]); // "r"

  //* TIPE DATA NUMBERS ==================
  // declare an integer
  int num1 = 10;

  // declare a double value
  double num2 = 10.5;
  // print the values

  print(num1); //10
  print(num2); //10.5

  //* Mengubah string menjadi integer ====
  print(num.parse('12')); //12
  print(num.parse('10.91')); //10.91

  // print(num.parse('12A')); // akan error jika dijalankan
  // print(num.parse('AAAA')); // karena teks tidak dapat di parse ke number

  // * Merubah dari int ke string
  int j = 45;
  String t = "$j";
  print("hello" + t);
}
