import 'package:flutter/material.dart';

class Pilihan {
  Pilihan({required this.teks, required this.warna});

  final String teks;
  final Color warna;
}
