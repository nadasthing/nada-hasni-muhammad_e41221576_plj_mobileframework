import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_gradient_pageview_dropdwon/halaman_dua.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<String> gambar = [
    'chimmy.gif',
    'cooky.gif',
    'koya.gif',
    'mang.gif',
    'rj.gif',
    'shooky.gif',
    'tata.gif',
    'van.gif',
  ];

  final Map<String, Color> colors = {
    'chimmy': const Color(0xFF2DB569),
    'cooky': const Color(0xFFF386B8),
    'koya': const Color(0xFF45CAF5),
    'mang': const Color(0xFFB19ECB),
    'rj': const Color(0xFFF58E4C),
    'shooky': const Color(0xFF46C1BE),
    'tata': const Color(0xFFFFEA0E),
    'van': const Color(0xFFD8E4E9),
  };

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;

    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
            colors: [
              Colors.white,
              Colors.purpleAccent,
              Colors.deepPurple,
            ],
          ),
        ),
        child: PageView.builder(
          controller: PageController(viewportFraction: 0.8),
          itemCount: gambar.length,
          itemBuilder: (context, index) => Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 50),
            child: Material(
              elevation: 8,
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Hero(
                    tag: gambar[index],
                    child: Material(
                      child: InkWell(
                        onTap: () => Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => HalamanDua(
                              gambar: gambar[index],
                              color: colors.values.elementAt(index),
                            ),
                          ),
                        ),
                        child: Container(
                          color: colors.values.elementAt(index),
                          child: Image.asset(
                            'assets/images/${gambar[index]}',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
