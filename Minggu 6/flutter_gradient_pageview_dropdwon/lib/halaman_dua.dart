import 'package:flutter/material.dart';
import 'package:flutter_gradient_pageview_dropdwon/pilihan.dart';

class HalamanDua extends StatefulWidget {
  const HalamanDua({
    Key? key,
    required this.gambar,
    required this.color,
  }) : super(key: key);

  final String gambar;
  final Color color;

  @override
  State<HalamanDua> createState() => _HalamanDuaState();
}

class _HalamanDuaState extends State<HalamanDua> {
  Color warna = Colors.grey;

  List<Pilihan> listPilihan = [
    Pilihan(teks: 'Red', warna: Colors.red),
    Pilihan(teks: 'Green', warna: Colors.green),
    Pilihan(teks: 'Blue', warna: Colors.blue),
  ];

  void _onSelectColor(Pilihan pilihan) {
    setState(() {
      warna = pilihan.warna;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('BT21'),
        backgroundColor: Colors.purpleAccent,
        actions: [
          PopupMenuButton<Pilihan>(
            onSelected: _onSelectColor,
            itemBuilder: (context) => listPilihan
                .map(
                  (pilihan) => PopupMenuItem<Pilihan>(
                    value: pilihan,
                    child: Text(pilihan.teks),
                  ),
                )
                .toList(),
          )
        ],
      ),
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: RadialGradient(
                center: Alignment.center,
                colors: [Colors.purple, warna, Colors.deepPurple],
              ),
            ),
          ),
          Center(
            child: Hero(
              tag: widget.gambar,
              child: ClipOval(
                child: SizedBox(
                  width: 200,
                  height: 200,
                  child: Material(
                    child: InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      child: Flexible(
                        flex: 1,
                        child: Container(
                          color: widget.color,
                          child: Image.asset(
                            'assets/images/${widget.gambar}',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
