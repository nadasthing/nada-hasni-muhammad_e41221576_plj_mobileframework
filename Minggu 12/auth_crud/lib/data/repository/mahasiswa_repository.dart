import 'dart:convert';
import 'dart:developer';
import 'package:auth_crud/data/local/session.dart';
import 'package:auth_crud/data/models/mahasiswa_model.dart';
import 'package:auth_crud/data/remote/api_endpoints.dart';
import 'package:auth_crud/data/responses/get_all_mahasiswa_response.dart';
import 'package:http/http.dart' as http;

class MahasiswaRepository {
  static const tag = 'MAHASISWA_REPOSITORY';

  static Future<List<MahasiswaModel>> getAllMahasiswa() async {
    try {
      final token = await Session.getToken();
      final uri = Uri.http(ApiEndpoints.baseUrl, ApiEndpoints.mahasiswa);
      final headers = {
        'Accept': 'application/json',
        'Authorization': token,
      };

      log('URI: ${uri.toString()}');
      log('headers: ${headers.toString()}');

      final response = await http.get(uri, headers: headers);
      final responseBody = jsonDecode(response.body);

      if (response.statusCode == 200) {
        return GetAllMahasiswaResponse.fromJson(responseBody).data;
      }

      throw responseBody['message'];
    } catch (e) {
      log(e.toString(), name: tag);
      rethrow;
    }
  }

  static Future<bool> deleteMahasiswa(int id) async {
    try {
      final token = await Session.getToken();
      final uri =
          Uri.http(ApiEndpoints.baseUrl, ApiEndpoints.detailMahasiswa(id));
      final headers = {
        'Accept': 'application/json',
        'Authorization': token,
      };

      log('URI: ${uri.toString()}');
      log('headers: ${headers.toString()}');

      final response = await http.delete(uri, headers: headers);
      final responseBody = jsonDecode(response.body);

      if (response.statusCode == 200) {
        return true;
      }

      throw responseBody['message'];
    } catch (e) {
      log(e.toString(), name: tag);
      rethrow;
    }
  }

  static Future<bool> createMahasiswa({
    required String name,
    required String nim,
    required String angkatan,
    required String jurusan,
  }) async {
    try {
      final token = await Session.getToken();
      final uri = Uri.http(ApiEndpoints.baseUrl, ApiEndpoints.mahasiswa);
      final headers = {
        'Accept': 'application/json',
        'Authorization': token,
      };
      final params = {
        'name': name,
        'nim': nim,
        'angkatan': angkatan,
        'jurusan': jurusan,
      };

      log('URI: ${uri.toString()}');
      log('headers: ${headers.toString()}');
      log('params: ${params.toString()}');

      final response = await http.post(uri, headers: headers, body: params);
      final responseBody = jsonDecode(response.body);

      if (response.statusCode == 201) {
        return true;
      }

      throw responseBody['errors'];
    } catch (e) {
      log(e.toString(), name: tag);
      rethrow;
    }
  }

  static Future<bool> updateMahasiswa({
    required int id,
    required String name,
    required String nim,
    required String angkatan,
    required String jurusan,
  }) async {
    try {
      final token = await Session.getToken();
      final uri =
          Uri.http(ApiEndpoints.baseUrl, ApiEndpoints.detailMahasiswa(id));
      final headers = {
        'Accept': 'application/json',
        'Authorization': token,
      };
      final params = {
        'name': name,
        'nim': nim,
        'angkatan': angkatan,
        'jurusan': jurusan,
      };

      log('URI: ${uri.toString()}');
      log('headers: ${headers.toString()}');
      log('params: ${params.toString()}');

      final response = await http.put(uri, headers: headers, body: params);
      final responseBody = jsonDecode(response.body);

      if (response.statusCode == 200) {
        return true;
      }

      throw responseBody['errors'];
    } catch (e) {
      log(e.toString(), name: tag);
      rethrow;
    }
  }
}
