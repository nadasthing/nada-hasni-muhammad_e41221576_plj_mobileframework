import 'package:auth_crud/data/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Session {
  static const token = 'TOKEN';
  static const isSignedIn = 'IS_SIGNED_IN';

  static const name = 'NAME';
  static const username = 'USERNAME';
  static const email = 'EMAIL';

  static Future<bool> checkIsSignedIn() async {
    final pref = await SharedPreferences.getInstance();
    return pref.getBool(isSignedIn) ?? false;
  }

  static Future<bool> saveSession({
    required String name,
    required String username,
    required String email,
    required String token,
  }) async {
    final pref = await SharedPreferences.getInstance();
    await pref.setString(Session.name, name);
    await pref.setString(Session.username, username);
    await pref.setString(Session.email, email);
    await pref.setString(Session.token, token);
    return await pref.setBool(Session.isSignedIn, true);
  }

  static Future<UserModel> getSignedinUser() async {
    final pref = await SharedPreferences.getInstance();
    final name = pref.getString(Session.name) ?? '';
    final username = pref.getString(Session.username) ?? '';
    final email = pref.getString(Session.email) ?? '';

    return UserModel(name: name, username: username, email: email);
  }

  static Future<String> getToken() async {
    final pref = await SharedPreferences.getInstance();
    final token = pref.getString(Session.token) ?? '';
    return 'Bearer $token';
  }

  static Future<bool> clearSession() async {
    final pref = await SharedPreferences.getInstance();
    return await pref.clear();
  }
}
