import 'dart:convert';

import 'package:auth_crud/data/models/mahasiswa_model.dart';

GetAllMahasiswaResponse getAllMahasiswaResponseFromJson(String str) =>
    GetAllMahasiswaResponse.fromJson(json.decode(str));

String getAllMahasiswaResponseToJson(GetAllMahasiswaResponse data) =>
    json.encode(data.toJson());

class GetAllMahasiswaResponse {
  GetAllMahasiswaResponse({
    required this.success,
    required this.message,
    required this.data,
  });

  bool success;
  String message;
  List<MahasiswaModel> data;

  factory GetAllMahasiswaResponse.fromJson(Map<String, dynamic> json) =>
      GetAllMahasiswaResponse(
        success: json["success"],
        message: json["message"],
        data: List<MahasiswaModel>.from(
            json["data"].map((x) => MahasiswaModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "message": message,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}
