class UserModel {
  UserModel({
    required this.name,
    required this.username,
    required this.email,
  });

  String name;
  String username;
  String email;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        name: json["name"],
        username: json["username"],
        email: json["email"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "username": username,
        "email": email,
      };
}
