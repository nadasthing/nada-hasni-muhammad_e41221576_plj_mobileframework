import 'package:auth_crud/data/repository/auth_repository.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  State<SignupScreen> createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final formKey = GlobalKey<FormState>();
  final inputName = TextEditingController();
  final inputUsername = TextEditingController();
  final inputEmail = TextEditingController();
  final inputPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const SizedBox(height: 80),
              const Text(
                'SIGN UP',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 32),
              TextFormField(
                controller: inputName,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(hintText: 'Name'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide name';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: inputUsername,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(hintText: 'Username'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide username';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                controller: inputEmail,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(hintText: 'Email'),
                validator: (value) {
                  final email = value.toString();
                  if (email.isEmpty || !GetUtils.isEmail(email)) {
                    return 'Please provide valid email';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                obscureText: true,
                controller: inputPassword,
                decoration: const InputDecoration(hintText: 'Password'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide password';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 24),
              ElevatedButton(
                onPressed: _doSignup,
                child: const Text('SIGN UP'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _doSignup() async {
    if (!formKey.currentState!.validate()) return;

    try {
      final result = await AuthRepository.signUp(
        name: inputName.text.trim(),
        username: inputUsername.text.trim(),
        email: inputEmail.text.trim(),
        password: inputPassword.text.trim(),
      );

      Fluttertoast.showToast(msg: result.message);
      Get.until((route) => route.isFirst);
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
    }
  }
}
