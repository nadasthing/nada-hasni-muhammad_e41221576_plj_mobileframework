import 'package:auth_crud/data/local/session.dart';
import 'package:auth_crud/data/models/mahasiswa_model.dart';
import 'package:auth_crud/data/repository/mahasiswa_repository.dart';
import 'package:auth_crud/routes/route_names.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Daftar mahasiswa'),
        actions: [
          IconButton(
            onPressed: _doLogout,
            icon: const Icon(Icons.logout),
          )
        ],
      ),
      body: FutureBuilder<List<MahasiswaModel>>(
          future: MahasiswaRepository.getAllMahasiswa(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            }

            if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }

            return RefreshIndicator(
              onRefresh: () async {
                setState(() {});
              },
              child: ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (contex, index) {
                  final mahasiswa = snapshot.data?[index];
                  return Slidable(
                    endActionPane:
                        ActionPane(motion: const ScrollMotion(), children: [
                      SlidableAction(
                        onPressed: (context) => _deleteMahasiswa(mahasiswa?.id),
                        backgroundColor: Colors.red,
                        foregroundColor: Colors.white,
                        icon: Icons.delete,
                        label: 'Delete',
                      ),
                    ]),
                    child: ListTile(
                      onTap: () => _toUpdate(mahasiswa),
                      title: Text('${mahasiswa?.name} (${mahasiswa?.nim})'),
                      subtitle: Text(
                          '${mahasiswa?.jurusan} - ${mahasiswa?.angkatan}'),
                    ),
                  );
                },
              ),
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: _toCreate,
        child: const Icon(Icons.add),
      ),
    );
  }

  _deleteMahasiswa(int? id) async {
    if (id == null) return;

    try {
      final result = await MahasiswaRepository.deleteMahasiswa(id);
      if (result) {
        return setState(() {});
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  void _toCreate() async {
    await Get.toNamed(RouteNames.createScreen);
    setState(() {});
  }

  void _toUpdate(MahasiswaModel? mahasiswa) async {
    await Get.toNamed(
      RouteNames.editScreen,
      arguments: {'mahasiswa': mahasiswa},
    );
    setState(() {});
  }

  void _doLogout() async {
    await Session.clearSession();
    Get.offAllNamed(RouteNames.signinScreen);
  }
}
