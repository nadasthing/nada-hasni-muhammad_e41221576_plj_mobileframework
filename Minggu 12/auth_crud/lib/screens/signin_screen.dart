import 'package:auth_crud/data/local/session.dart';
import 'package:auth_crud/data/repository/auth_repository.dart';
import 'package:auth_crud/routes/route_names.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class SigninScreen extends StatefulWidget {
  const SigninScreen({Key? key}) : super(key: key);

  @override
  State<SigninScreen> createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  final formKey = GlobalKey<FormState>();
  final inputUsername = TextEditingController();
  final inputPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'SIGN IN',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 32),
              TextFormField(
                controller: inputUsername,
                textInputAction: TextInputAction.next,
                decoration: const InputDecoration(hintText: 'Username'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide username';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                obscureText: true,
                controller: inputPassword,
                decoration: const InputDecoration(hintText: 'Password'),
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please provide password';
                  }

                  return null;
                },
              ),
              const SizedBox(height: 24),
              ElevatedButton(
                onPressed: _doSignin,
                child: const Text('SIGN IN'),
              ),
              const SizedBox(height: 16),
              ElevatedButton(
                onPressed: () => Get.toNamed(RouteNames.signupScreen),
                style: ElevatedButton.styleFrom(backgroundColor: Colors.green),
                child: const Text('SIGN UP'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _doSignin() async {
    if (!formKey.currentState!.validate()) return;

    try {
      final result = await AuthRepository.signIn(
        username: inputUsername.text.trim(),
        password: inputPassword.text.trim(),
      );

      await Session.saveSession(
        name: result.data.name,
        username: result.data.username,
        email: result.data.email,
        token: result.token,
      );

      Get.offNamed(RouteNames.homeScreen);
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
    }
  }
}
