import 'package:auth_crud/data/local/session.dart';
import 'package:auth_crud/routes/route_names.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  _navigateNext() async {
    await Future.delayed(const Duration(seconds: 3));
    final isSignedIn = await Session.checkIsSignedIn();
    if (isSignedIn) {
      return Get.offNamed(RouteNames.homeScreen);
    }

    Get.offNamed(RouteNames.signinScreen);
  }

  @override
  Widget build(BuildContext context) {
    _navigateNext();

    return const Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Text(
          'AUTH CRUD EXAMPLE',
          style: TextStyle(
            fontSize: 32,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
